const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');

const app = express();
const PORT = process.env.PORT || 5000;



mongoose.connect(process.env.MONGODB || "mongodb+srv://admin:admin123@cluster0.4qaon.mongodb.net/capstone3?retryWrites=true&w=majority",
{


	useNewUrlParser: true,
	useUnifiedTopology: true

});

let db = mongoose.connection;
db.on('error',console.error.bind(console, "MongoDB Connection Error."));
db.once('open',()=>console.log("Connected to MongoDB."))

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use('/products',productRoutes);

const orderRoutes = require("./routes/orderRoutes");
app.use('/orders',orderRoutes);


app.listen(PORT, () => console.log(`Server is running at port ${port} Chong!!`));