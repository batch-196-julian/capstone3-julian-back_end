const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    imageURL: {
        type: String,
        default: null
    },

    name:{
        type: String, required: [true, "Product Name is required."]
    },
    description:{
        type: String, required: [true, "Product Description is required."]
    },
    price:{
        type: Number, required: [true, "Product price is required."]
    },
    stocks: {
        type: Number,
        required: [true, "Please input number of quantity available."]
    },
    isActive:{
        type: Boolean, default: true
    },
    createdOn:{
        type: Date, default: new Date()
    },

})

module.exports = mongoose.model("Product", productSchema);
